import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here
        //
		// ********** The Answer ************
		// we will get the capicty int of the FloatBuffer by counting the vertices
		// we can do that by getting the capicity of the vertices through the getVertices FloatBuffer.
		int capicty = (Main.vertices.capacity()) / 3;
		// We will have to get the centerpoint of the Obj model
		// to do this we will get the sum of all the x values, then devide on the vertices count. and repeat this with the y and Z values.
		float xCenterPointValue = 0;
		float yCenterPointValue = 0;
		float zCenterPointValue = 0;
		for (int i=0; i<capicty; i++)
        {
		xCenterPointValue += buf[i * 3];
		yCenterPointValue += buf[i * 3 + 1];
		zCenterPointValue += buf[i * 3 + 2];
		}
		xCenterPointValue /= capicty;
		yCenterPointValue /= capicty;
		zCenterPointValue /= capicty;
		// Now we will subtract the center point coordinate from each coordinate in the same axis and assign this to the FloatBuffer values
		for (int s=0; s<capicty; s++)
        {
		buf[i * 3] -= xCenterPointValue;
		buf[i * 3 + 1] -= yCenterPointValue;
		buf[i * 3 + 2] -= zCenterPointValue;
		}
    }
}
